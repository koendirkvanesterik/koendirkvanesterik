# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [3.2.0](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/compare/v3.1.0...v3.2.0) (2020-03-20)

### Features

- add profile component with redux implementation ([56ec516](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/56ec5160dd99c300771e9a9e49b47e8a1470000d))

### Bug Fixes

- remove obsolete canonical link implementation ([c4324c0](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/c4324c094f1ae932035e058c90e2e105b234e0ef))

## [3.1.0](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/compare/v3.0.0...v3.1.0) (2020-03-18)

### Features

- add metadata component ([5b3861f](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/5b3861f89946a7f3be8c0c86fa303b29f9442f28))
- implement google analytics ([a58d672](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/a58d672e23ccc0de9797cd8ccb2fde9b93643582))
- implement redux store ([1800ad5](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/1800ad57bac367103b34cf67e5eb49ceed1042d4))

### Bug Fixes

- reduce fout by aligning fonts ([e2e189c](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/e2e189c4d43aa21e5121cd8fe563a74dff3e4491))
- reduce fout by aligning fonts ([941f7c7](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/941f7c737d173260bd5f4af737fc0ecdc05fdb12))
- reduce fout by aligning fonts ([89ea85d](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/89ea85dc284a71b7c65ae6486bf88493cd3c63a8))

## [3.0.0](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/compare/v2.2.0...v3.0.0) (2020-03-16)

### ⚠ BREAKING CHANGES

- revamp of entire micro site

### Features

- **atoms:** restructure theme component ([234a17d](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/234a17d8a05b218fe663eee0e1ad4feedf89aaf2))
- **atoms:** update theme global setup ([21334b7](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/21334b78082c27ed6760e752c17629816536217d))
- **data:** restructure various data files ([10d4098](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/10d40987a63e6328449b31868918fc607dbe19b1))
- **mocules:** add content component ([e46c392](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/e46c39275c3f745ca23444fab336dd30e8553422))
- **molecules:** add border component ([fe2dfc2](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/fe2dfc23236ee8ff038e7ef5ca6eb872939f0e42))
- **molecules:** add link component ([b3b11fa](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/b3b11fa026f1edd8a0810aac7d5a2c568005e3ab))
- **molecules:** add notification component ([1c56b6b](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/1c56b6bce107926487829ac72cb7620f6f3dea32))
- **molecules:** add padding to main component ([49f541d](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/49f541d07a71c3b66cec16c34ea45b22c6c6d170))
- **molecules:** finetune unit tests for link component ([51577fd](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/51577fdf4706a58b2cde990911829df9fff6aa2f))
- **molecules:** finetune unit tests for text and heading components ([bb4cb77](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/bb4cb77edc6615f663f8f493caf113b16d9a3a64))
- **molecules:** restructure variables to seperate file ([bc75b60](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/bc75b60d26c5c62f148f61a5adf3b52ebb808909))
- define renewed game layout of micro site ([014e108](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/014e10839aa04860079ec734a3dac40d073ea8da))
- **organisms:** intergrate revised organism interface in factory ([f0385e6](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/f0385e682fbc75add50e9c5c6f41426b192f9742))
- redesign favicon ([32213cc](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/32213cc06fadbdc6d529a38543e7f50ef4a3238c))
- **organisms:** add bottom margin to header component ([a8f57ae](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/a8f57ae21707a6fbb1ae419786e8103b95ef3de5))
- **organisms:** add centralized dictionary component ([428988f](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/428988f9496717cd8f79864a323ec28630dc73ed))
- **organisms:** add cover component ([e4c3acf](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/e4c3acf6965e88242c6b15c1d32869fe2e3f6ab3))
- **organisms:** add main data structure to root component ([93b704d](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/93b704d99974165749342e541f8cfc4103d96a81))
- **organisms:** add overview component ([7713bab](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/7713bab377486499fa7e1e1ab6aa92e646aba00d))
- **organisms:** add paragraph component ([af994a7](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/af994a76021dc939897aca6ba48c9f67de5c5799))
- **organisms:** add top margin to footer component ([9a58efc](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/9a58efc1f7f9ce6dd0159217c1d5b91124a016c0))
- **organisms:** align color scheme in multiple elements ([4f5dd73](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/4f5dd73a99d7c56bc5edc8210c24a441885cac04))
- **organisms:** delete obsolete content component ([912328e](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/912328ef8f960bfd7751615380971d6cc165f00a))
- **organisms:** finetune paragraph component ([bb8c522](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/bb8c522a2bd665952779b9745efcff2c7ff24a22))
- **organisms:** finetune various organism components ([7d82647](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/7d826479a09956f928aa14fc469a3f4aedf66f2e))
- **organisms:** implement dictionary component ([d35a3aa](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/d35a3aa2f555512e1cc291c5a473652acb8ee8cd))
- **organisms:** integrate content component into paragraph component ([d51bfc1](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/d51bfc184dd3c4fe115c0c8736d69aa2b86495e9))
- **organisms:** integrate content compontent into columns component ([ab4f52a](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/ab4f52a16ba62f1253794a93a744921845de0497))
- **organisms:** rename type property of theme component ([f8b981c](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/f8b981c17ebeed36dbe3e5248e4cea5f8a32a78b))
- **organisms:** restructure factory component ([98e49fd](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/98e49fd4815118e830083d5755199add48f26b58))
- **project:** renew favico image ([9b82913](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/9b82913abb546618d38e6294ae199cdbbf785b59))
- **providers:** add library component to providers folder ([359da16](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/359da164fecdc3ec7f7b355951d2c0b48e1d3ffa))
- **providers:** relocate dictionary to providers folder ([293d500](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/293d500562163fadf81f347369ae6412b78dc888))
- **providers:** relocate theme component to providers folder ([16235a6](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/16235a631e9f16d9ae82c4af287d5b6aeda53e91))
- **templates:** implement factory component with route data ([0e84467](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/0e84467b7fdc42cc9a4e6b4829cba83c6be85db7))
- **utils:** relocate link utils to global folder ([55ee27b](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/55ee27b9ef7fe6c619c7999706ba5caa03661726))

### Bug Fixes

- add additional step to checkout master branch in order to push ([6856f11](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/6856f11a534256a8b61293238fa3830de33a18fd))
- add use name and email to git config of tag step ([8fe2dd7](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/8fe2dd7474b79d7f3c0385ef55f31c44329b7d0e))
- format unpretty files ([b05c23c](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/b05c23cb83a190ce39beba358002b8e8d23be56d))

* revamp of entire micro site ([dddf713](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/dddf71320756ff294e004940d1cd13da89b35378))

<a name="2.3.0"></a>

# [2.3.0](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/compare/v2.2.0...v2.3.0) (2019-05-11)

### Bug Fixes

- add additional step to checkout master branch in order to push ([6856f11](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/6856f11))
- add use name and email to git config of tag step ([8fe2dd7](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/8fe2dd7))
- format unpretty files ([b05c23c](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/b05c23c))
- prettify gitlab ci script ([7719884](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/7719884))
- remove illegal flag atomic ([3092f70](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/3092f70))
- try to push commit to master when prepping release [#1](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/issues/1) ([e74c6fe](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/e74c6fe))
- try to push commit to master when prepping release [#10](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/issues/10) ([3ae4570](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/3ae4570))
- try to push commit to master when prepping release [#3](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/issues/3) ([0f9a15e](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/0f9a15e))
- try to push commit to master when prepping release [#4](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/issues/4) ([f52251c](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/f52251c))
- try to push commit to master when prepping release [#5](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/issues/5) ([12cd522](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/12cd522))
- try to push commit to master when prepping release [#6](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/issues/6) ([eaab5c3](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/eaab5c3))
- try to push commit to master when prepping release [#7](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/issues/7) ([ef84d38](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/ef84d38))
- try to push commit to master when prepping release [#8](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/issues/8) ([1e0d24e](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/1e0d24e))
- try to push commit to master when prepping release [#9](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/issues/9) ([0eb46a1](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/0eb46a1))

### Features

- **mocules:** add content component ([e46c392](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/e46c392))
- **molecules:** add link component ([b3b11fa](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/b3b11fa))
- **molecules:** add padding to main component ([49f541d](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/49f541d))
- **molecules:** restructure variables to seperate file ([bc75b60](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/bc75b60))
- **organisms:** add bottom margin to header component ([a8f57ae](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/a8f57ae))
- **organisms:** add main data structure to root component ([93b704d](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/93b704d))
- **organisms:** add paragraph component ([af994a7](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/af994a7))
- **organisms:** add top margin to footer component ([9a58efc](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/9a58efc))
- **organisms:** align color scheme in multiple elements ([4f5dd73](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/4f5dd73))
- **organisms:** integrate content component into paragraph component ([d51bfc1](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/d51bfc1))
- **organisms:** integrate content compontent into columns component ([ab4f52a](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/ab4f52a))
- **organisms:** intergrate revised organism interface in factory ([f0385e6](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/f0385e6))
- **organisms:** rename type property of theme component ([f8b981c](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/f8b981c))
- **utils:** relocate link utils to global folder ([55ee27b](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/55ee27b))

<a name="2.2.0"></a>

# 2.2.0 (2019-05-03)

### Bug Fixes

- add mailto prefix to mail link ([cc2f2e5](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/cc2f2e5))
- adjust coverage command to exclude tests folders ([77094ec](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/77094ec))
- prettify gitlab-ci.yml file ([cc727b3](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/cc727b3))
- reduce format check command to relevant files ([4bab5e6](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/4bab5e6))
- remove path prefix to point to correct resources ([95b1eaa](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/95b1eaa))

### Features

- **atoms:** add various config files ([d6fc16a](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/d6fc16a))
- **global:** add manifest plugin and icon image ([df6b5b6](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/df6b5b6))
- **molecules:** add logo component ([63840b8](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/63840b8))
- **molecules:** add main component ([28eb2f9](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/28eb2f9))
- **molecules:** add theme high order component ([5654c08](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/5654c08))
- **molecules:** refine spacing logo component ([b9f65a9](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/b9f65a9))
- **molecules:** refine theme styles ([5f1b93c](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/5f1b93c))
- **organisms:** add columns component ([f4ce647](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/f4ce647))
- **organisms:** add factory component ([e2be32c](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/e2be32c))
- **organisms:** add footer component ([382f8d5](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/382f8d5))
- **organisms:** add footer data model ([0d6eab6](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/0d6eab6))
- **organisms:** add header component ([bf828eb](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/bf828eb))
- **organisms:** add nested children to footer component ([7472f19](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/7472f19))
- **organisms:** add nested children to header component ([eac94bf](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/eac94bf))
- **organisms:** add root component ([8e1edf5](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/8e1edf5))
- **organisms:** integrate factory component ([4309956](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/4309956))

<a name="2.1.0"></a>

# 2.1.0 (2019-05-03)

### Bug Fixes

- add mailto prefix to mail link ([cc2f2e5](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/cc2f2e5))
- adjust coverage command to exclude tests folders ([77094ec](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/77094ec))
- prettify gitlab-ci.yml file ([cc727b3](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/cc727b3))
- reduce format check command to relevant files ([4bab5e6](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/4bab5e6))
- remove path prefix to point to correct resources ([95b1eaa](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/95b1eaa))

### Features

- **atoms:** add various config files ([d6fc16a](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/d6fc16a))
- **global:** add manifest plugin and icon image ([df6b5b6](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/df6b5b6))
- **molecules:** add logo component ([63840b8](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/63840b8))
- **molecules:** add main component ([28eb2f9](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/28eb2f9))
- **molecules:** add theme high order component ([5654c08](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/5654c08))
- **molecules:** refine spacing logo component ([b9f65a9](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/b9f65a9))
- **molecules:** refine theme styles ([5f1b93c](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/5f1b93c))
- **organisms:** add columns component ([f4ce647](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/f4ce647))
- **organisms:** add factory component ([e2be32c](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/e2be32c))
- **organisms:** add footer component ([382f8d5](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/382f8d5))
- **organisms:** add footer data model ([0d6eab6](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/0d6eab6))
- **organisms:** add header component ([bf828eb](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/bf828eb))
- **organisms:** add nested children to footer component ([7472f19](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/7472f19))
- **organisms:** add nested children to header component ([eac94bf](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/eac94bf))
- **organisms:** add root component ([8e1edf5](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/8e1edf5))
- **organisms:** integrate factory component ([4309956](https://gitlab.com/koendirkvanesterik/koendirkvanesterik/commit/4309956))
