import React, { FunctionComponent } from 'react'

import { Section } from 'components/organisms/Section'

const NotFound: FunctionComponent = () => <Section name="Not Found" />

/**
 * The `NotFound` page component needs to be exported by default, in order for
 * the script in `gatsby-node.js` to resolve it.
 */
export default NotFound
