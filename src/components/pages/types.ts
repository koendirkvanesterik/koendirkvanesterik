export interface PageProps {
  /**
   * String value representing path of page
   */
  path: string
}
