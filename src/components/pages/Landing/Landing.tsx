import React, { FunctionComponent } from 'react'

import { Cockpit } from 'components/organisms/Cockpit'
import { Section } from 'components/organisms/Section'

const Landing: FunctionComponent = () => (
  <>
    <Cockpit />
    <Section name="Summary" />
    <Section name="Contact" />
  </>
)
/**
 * The `Landing` page component needs to be exported by default, in order for
 * the script in `gatsby-node.js` to resolve it.
 */
export default Landing
