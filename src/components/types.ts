import { ProfileState } from 'components/organisms/Profile/types'

export interface ComponentState {
  /**
   * Object holding all properties for Profile component
   */
  profile: ProfileState
}
