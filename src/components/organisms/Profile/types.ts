import { AxiosResponse } from 'axios'

export interface ProfileState {
  /**
   * Object of type data property of axios response interface
   */
  data: AxiosResponse['data']
  /**
   * Boolean value determining whether load is in progress
   */
  isLoading: boolean
  /**
   * Object of type status property of axios response interface
   */
  status: AxiosResponse['status']
}
