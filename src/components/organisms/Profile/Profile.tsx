import React, { FunctionComponent, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { Label } from 'components/organisms/Dictionary'

import { fetchProfile } from './actions'
import { Loader } from './components/Loader'
import {
  selectJobTitle,
  selectLoadingState,
  selectProfileStatus,
} from './selectors'

export const Profile: FunctionComponent = () => {
  /**
   * Declare instance of state dispatcher using a hook
   */
  const dispatch = useDispatch()
  /**
   * Get various properties from profile part of state
   */
  const isLoading = useSelector(selectLoadingState)
  const status = useSelector(selectProfileStatus)
  const jobTitle = useSelector(selectJobTitle)
  /**
   * Execute possible fetch sequence at client side
   */
  useEffect(() => {
    /**
     * Check if status is defined, if not - dispatch profile fetch action
     */
    if (!status) {
      dispatch(fetchProfile())
    }
    /**
     * Set depending variables array as second argument which informs useEffect
     * hook to only execute once
     */
  }, [dispatch, status])
  /**
   * If in the middle of loading - show loading text
   */
  if (isLoading) {
    return <Loader />
  }
  /**
   * If loading was successful - display earlier selected headline
   * In case no headline is available, fallback to default text
   */
  if (status === 200) {
    return jobTitle ? (
      <>{jobTitle}</>
    ) : (
      <Label id="header.profile.default.text" />
    )
  }
  /**
   * If loading failed - display error text
   */
  if (status === 404) {
    return <Label id="header.profile.error.text" />
  }
  /**
   * Return line break by default to make sure layout is not jumping upwards in
   * between states
   */
  return <br />
}

Profile.displayName = 'Profile'
