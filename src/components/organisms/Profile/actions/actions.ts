import { AxiosResponse } from 'axios'

import {
  ActionTypes,
  FetchProfileAction,
  FetchProfileSuccessAction,
  FetchProfileErrorAction,
  SetLoadingAction,
} from './types'

/**
 * Action dispatched when profile should be fetched
 */
export const fetchProfile = (): FetchProfileAction => ({
  type: ActionTypes.FETCH_PROFILE,
})
/**
 * Action dispatched when profile is fetched successfully
 */
export const fetchProfileSuccess = ({
  data,
  status,
}: AxiosResponse): FetchProfileSuccessAction => ({
  type: ActionTypes.FETCH_PROFILE_SUCCESS,
  payload: { data, status },
})
/**
 * Action dispatched when profile failed to be fetched
 */
export const fetchProfileError = (): FetchProfileErrorAction => ({
  type: ActionTypes.FETCH_PROFILE_ERROR,
})
/**
 * Action dispatched when api tasks are loading ie. fetching profile, etc.
 */
export const setLoading = (isLoading: boolean): SetLoadingAction => ({
  type: ActionTypes.SET_LOADING,
  payload: { isLoading },
})
