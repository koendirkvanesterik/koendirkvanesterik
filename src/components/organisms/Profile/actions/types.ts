import { AxiosResponse } from 'axios'

/**
 * Enum holding all Profile component action types
 */
export enum ActionTypes {
  FETCH_PROFILE = '@@profile/FETCH_PROFILE',
  FETCH_PROFILE_SUCCESS = '@@profile/FETCH_PROFILE_SUCCESS',
  FETCH_PROFILE_ERROR = '@@profile/FETCH_PROFILE_ERROR',
  SET_LOADING = '@@profile/SET_LOADING',
}

export interface FetchProfileAction {
  /**
   * Type defining action
   */
  type: typeof ActionTypes.FETCH_PROFILE
}

export interface FetchProfileSuccessAction {
  /**
   * Type defining action
   */
  type: typeof ActionTypes.FETCH_PROFILE_SUCCESS
  /**
   * Object holding to be updated properties
   */
  payload: {
    /**
     * Object of type data property of axios response interface
     */
    data: AxiosResponse['data']
    /**
     * Object of type status property of axios response interface
     */
    status: AxiosResponse['status']
  }
}

export interface FetchProfileErrorAction {
  /**
   * Type defining action
   */
  type: typeof ActionTypes.FETCH_PROFILE_ERROR
}

export interface SetLoadingAction {
  /**
   * Type defining action
   */
  type: typeof ActionTypes.SET_LOADING
  /**
   * Object holding to-be-updated properties
   */
  payload: {
    /**
     * Boolean value defining whether loading is in progress
     */
    isLoading: boolean
  }
}

export type ProfileActions =
  | FetchProfileAction
  | FetchProfileSuccessAction
  | FetchProfileErrorAction
  | SetLoadingAction
