import React, { FunctionComponent } from 'react'
import { Box } from 'rebass'

import { Label } from 'components/organisms/Dictionary'
import { LOADING_TIME } from 'components/organisms/Profile/constants'

import { blink } from './styles'

export const Loader: FunctionComponent = () => (
  <>
    <Label id="header.profile.loading.text" />
    <Box
      display="inline-block"
      height="18px"
      overflow="hidden"
      sx={{
        animationDuration: `${LOADING_TIME}ms`,
        animationIterationCount: 'infinite',
        animationName: blink,
        animationTimingFunction: 'steps(4, end)',
        letterSpacing: '1px',
      }}
      width={0}
    >
      ...
    </Box>
  </>
)

Loader.displayName = 'Loader'
