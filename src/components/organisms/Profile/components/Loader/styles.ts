import { keyframes } from '@emotion/core'

export const blink = keyframes`
  to {
    width: 24px;
  }
`
