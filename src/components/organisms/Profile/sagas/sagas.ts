import Axios from 'axios'
import { SagaIterator } from 'redux-saga'
import { call, delay, put, takeLatest } from 'redux-saga/effects'

import {
  fetchProfileError,
  fetchProfileSuccess,
  setLoading,
} from 'components/organisms/Profile/actions'
import { ActionTypes } from 'components/organisms/Profile/actions/types'
import { LOADING_TIME } from 'components/organisms/Profile/constants'

export function* fetchProfile(): SagaIterator {
  /**
   * Set loading state of Profile component to true
   */
  yield put(setLoading(true))
  /**
   * A little bit of delay - just because we want to show off
   */
  yield delay(LOADING_TIME)
  /**
   * Try-catch logic to fetch in case of error handling
   */
  try {
    /**
     * Request profile from api
     */
    const result = yield call(
      Axios.get,
      process.env.GATSBY_PROFILE_API_RESOURCE || '',
    )
    /**
     * Dispatch success action if fetching profile succeeded
     */
    yield put(fetchProfileSuccess(result))
  } catch (error) {
    /**
     * Dispatch error action if fetching profile failed
     */
    yield put(fetchProfileError())
  }
  /**
   * Set loading state of Profile component back to false
   */
  yield put(setLoading(false))
}

export const sagas = [takeLatest(ActionTypes.FETCH_PROFILE, fetchProfile)]
