import { createSelector } from 'reselect'

import { ProfileState } from 'components/organisms/Profile/types'

import { State } from 'providers/store/types'

/**
 * Main selector for profile component
 */
export const selectProfileState = (state: State): ProfileState => state.profile
/**
 * Selector to get loading state from profile part of state
 */
export const selectLoadingState = createSelector(
  selectProfileState,
  profileState => profileState.isLoading,
)
/**
 * Selector to get status property from profile part of state
 */
export const selectProfileStatus = createSelector(
  selectProfileState,
  profileState => profileState.status,
)
/**
 * Selector to get data property from profile part of state
 */
export const selectProfileData = createSelector(
  selectProfileState,
  profileState => profileState.data,
)
/**
 * Selector to get job title property from profile data
 */
export const selectJobTitle = createSelector(
  selectProfileData,
  profileData => profileData?.job_title,
)
