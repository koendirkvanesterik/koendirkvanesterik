import {
  ActionTypes,
  ProfileActions,
} from 'components/organisms/Profile/actions/types'
import { ProfileState } from 'components/organisms/Profile/types'

/**
 * Define default state of `Profile` component
 */
export const defaultState: ProfileState = {
  data: undefined,
  isLoading: false,
  status: undefined,
}

export const reducers = (
  state: ProfileState = defaultState,
  action: ProfileActions,
): ProfileState => {
  /**
   * Switch-case statement in order to differentiate between action types
   */
  switch (action.type) {
    case ActionTypes.FETCH_PROFILE_SUCCESS:
      /**
       * Return and spread initial state and updated cart properties
       */
      return {
        ...state,
        data: action.payload.data,
        status: action.payload.status,
      }

    case ActionTypes.FETCH_PROFILE_ERROR:
      /**
       * Return and spread initial state and updated cart properties
       */
      return { ...state, status: 404 }

    case ActionTypes.SET_LOADING:
      /**
       * Return and spread initial state with updated variables from payload
       */
      return { ...state, isLoading: action.payload.isLoading }

    default:
      /**
       * Return default state just in case
       */
      return state
  }
}
