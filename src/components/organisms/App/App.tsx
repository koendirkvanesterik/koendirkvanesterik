import React, { FunctionComponent } from 'react'
import { Provider } from 'react-redux'
import { Flex } from 'rebass'

import { Theme } from 'components/atoms/Theme'

import { Dictionary } from 'components/organisms/Dictionary'
import { Footer } from 'components/organisms/Footer'
import { Header } from 'components/organisms/Header'
import { Main } from 'components/organisms/Main'
import { Metadata } from 'components/organisms/Metadata'

import { store } from 'providers/store'

export const App: FunctionComponent = ({ children }) => (
  <Provider store={store()}>
    <Dictionary>
      <Theme>
        <Metadata />
        <Flex flexDirection="column" minHeight="100vh">
          <Header />
          <Main>{children}</Main>
          <Footer />
        </Flex>
      </Theme>
    </Dictionary>
  </Provider>
)

App.displayName = 'App'
