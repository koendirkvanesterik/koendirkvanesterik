import { shallow } from 'enzyme'
import React from 'react'

import { App } from './App'

describe('Root', () => {
  it('should render as expected', () => {
    const component = shallow(<App />)
    // expect(component).toMatchSnapshot()
    expect(component.find('Header').exists()).toBe(true)
    expect(component.find('Main').exists()).toBe(true)
    expect(component.find('Footer').exists()).toBe(true)
  })
})
