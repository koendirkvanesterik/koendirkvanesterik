/**
 * Type needed to extend component types within `Library` provider
 */
export type ComponentTypes = 'Cover' | 'Overview' | 'Paragraph'
