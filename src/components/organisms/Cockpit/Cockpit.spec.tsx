import { shallow } from 'enzyme'
import React from 'react'
import { Box } from 'rebass'

import { Cockpit } from './Cockpit'

describe('Root', () => {
  it('should render as expected', () => {
    const component = shallow(<Cockpit />)
    // expect(component).toMatchSnapshot()
    expect(component.find('Wrapper').exists()).toBe(true)
    expect(component.find(Box).exists()).toBe(true)
    expect(component.find(Box).length).toBe(3)
  })
})
