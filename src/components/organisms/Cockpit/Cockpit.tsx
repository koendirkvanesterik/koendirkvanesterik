import React, { FunctionComponent } from 'react'
import { Box } from 'rebass'

import { colors } from 'components/atoms/Theme/presets/colors'
import { Wrapper } from 'components/molecules/Wrapper'

export const Cockpit: FunctionComponent = () => (
  <Wrapper width={1}>
    <Box
      marginBottom={4}
      sx={{
        border: `1px solid ${colors.green[0]}`,
        lineHeight: 0,
      }}
    >
      <Box
        display="inline-block"
        height="320px"
        width="50%"
        sx={{
          background: `repeating-linear-gradient(-45deg, transparent, transparent 50px, ${colors.green[0]} 50px, ${colors.green[0]} 100px)`,
        }}
      />
      <Box
        display="inline-block"
        height="320px"
        width="50%"
        sx={{
          background: `repeating-linear-gradient(45deg, transparent, transparent 50px, ${colors.green[0]} 50px, ${colors.green[0]} 100px)`,
        }}
      />
    </Box>
  </Wrapper>
)

Cockpit.displayName = 'Cockpit'
