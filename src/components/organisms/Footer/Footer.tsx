import React from 'react'
import { Box } from 'rebass'

import { Wrapper } from 'components/molecules/Wrapper'

import { Copyright } from './components/Copyright/Copyright'

export const Footer: React.FunctionComponent = React.memo(() => (
  <Box as="footer" paddingBottom={1} paddingTop={4}>
    <Wrapper>
      <Copyright />
    </Wrapper>
  </Box>
))

Footer.displayName = 'Footer'
