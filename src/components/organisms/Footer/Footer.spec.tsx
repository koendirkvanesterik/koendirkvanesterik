import { shallow } from 'enzyme'
import React from 'react'
import { Box } from 'rebass'

import { Footer } from './Footer'

describe('Footer', () => {
  it('should render as expected', () => {
    const component = shallow(<Footer />)
    // expect(component).toMatchSnapshot()
    expect(component.find(Box).exists()).toBe(true)
    expect(component.find('Wrapper').exists()).toBe(true)
    expect(component.find('Copyright').exists()).toBe(true)
  })
})
