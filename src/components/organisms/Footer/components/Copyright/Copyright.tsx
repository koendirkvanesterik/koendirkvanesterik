import React, { FunctionComponent } from 'react'
import { Text } from 'rebass'

import { Label } from 'components/organisms/Dictionary'

export const Copyright: FunctionComponent = () => (
  <Text as="p" fontSize={0}>
    <Label
      id="footer.copyright.text"
      replacement={{ year: new Date().getFullYear() }}
    />
  </Text>
)

Copyright.displayName = 'Copyright'
