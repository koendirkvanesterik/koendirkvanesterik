import { shallow } from 'enzyme'
import React from 'react'

import { Copyright } from './Copyright'

describe('Copyright', () => {
  it('should render as expected', () => {
    expect(shallow(<Copyright />)).toMatchSnapshot()
  })
})
