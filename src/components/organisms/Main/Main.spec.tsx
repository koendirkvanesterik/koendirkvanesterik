import { shallow } from 'enzyme'
import React from 'react'
import { Flex } from 'rebass'

import { Main } from './Main'

describe('Main', () => {
  it('should render as expected', () => {
    const component = shallow(<Main />)
    // expect(component).toMatchSnapshot()
    expect(component.find(Flex).exists()).toBe(true)
  })
})
