import React, { FunctionComponent } from 'react'
import { Flex } from 'rebass'

export const Main: FunctionComponent = ({ children }) => (
  <Flex as="main" flex="1 1 auto" flexDirection="column">
    {children}
  </Flex>
)

Main.displayName = 'Main'
