import React from 'react'
import { Box } from 'rebass'

import { Wrapper } from 'components/molecules/Wrapper'
import { Profile } from 'components/organisms/Profile'

import { Logo } from './components/Logo'

export const Header: React.FunctionComponent = () => (
  <Box as="header" paddingBottom={4} paddingTop={2}>
    <Wrapper>
      <Logo />
      <Profile />
    </Wrapper>
  </Box>
)

Header.displayName = 'Header'
