import { shallow } from 'enzyme'
import React from 'react'
import { Heading } from 'rebass'

import { Logo } from './Logo'

describe('Logo', () => {
  const component = shallow(<Logo />)
  it('should render as expected', () => {
    // expect(component).toMatchSnapshot()
    expect(component.find(Heading).exists()).toBeTruthy()
    expect(component.find('Link').exists()).toBeTruthy()
    expect(component.find('Label').exists()).toBeTruthy()
  })
})
