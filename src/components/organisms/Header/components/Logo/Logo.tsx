import React, { FunctionComponent } from 'react'
import { Heading } from 'rebass'

import { Link } from 'components/atoms/Link/Link'

import { Label } from 'components/organisms/Dictionary'

export const Logo: FunctionComponent = () => (
  <Heading as="h1" fontSize={2} sx={{ wordSpacing: '3px' }}>
    <Link href="/" variant="heading">
      <Label id="header.logo.text" />
    </Link>
  </Heading>
)

Logo.displayName = 'Logo'
