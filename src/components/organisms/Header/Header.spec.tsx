import { shallow } from 'enzyme'
import React from 'react'

import { Header } from './Header'

describe('Header', () => {
  it('should render as expected', () => {
    const component = shallow(<Header />)
    // expect(component).toMatchSnapshot()
    expect(component.find('Logo').exists()).toBe(true)
    expect(component.find('Profile').exists()).toBe(true)
  })
})
