import { shallow } from 'enzyme'
import React from 'react'

import { data as metadata } from './hooks/useMetadata/__stubs__/data'
import * as hooks from './hooks/useMetadata'

import { Metadata } from './Metadata'

const useMetadata = jest.spyOn(hooks, 'useMetadata')
useMetadata.mockImplementation(() => ({
  ...metadata,
}))

describe('Metadata', () => {
  const component = shallow(<Metadata />)
  it('should render as expected', () => {
    // expect(component).toMatchSnapshot()
    expect(component.find('HelmetWrapper').exists()).toBeTruthy()
    expect(component.find('title').exists()).toBeTruthy()
    expect(component.find('meta').length).toBe(12)
  })
})
