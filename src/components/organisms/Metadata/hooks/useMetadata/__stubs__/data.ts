import { Metadata } from '../types'

export const data: Metadata = {
  title: 'Foo Bar',
  description: 'Nisi reprehenderit incididunt duis duis consequat.',
  keywords:
    'massa, placerat, duis, ultricies, lacus, sed, turpis, tincidunt, id, aliquet, risus, feugiat, in, ante, metus, dictum, at, tempor, commodo, ullamcorper',
  image: {
    fixed: {
      src: 'https://foo.bar/baz.png',
      width: '210',
      height: '90',
    },
  },
  url: 'https://foo.bar',
}
