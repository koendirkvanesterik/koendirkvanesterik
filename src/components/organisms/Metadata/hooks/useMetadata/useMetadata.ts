import { graphql, useStaticQuery } from 'gatsby'

import { Metadata, MetadataData } from './types'

/**
 * Use static query to retrieve metadata from config
 */
export const useMetadata = (): Metadata => {
  /**
   * Directly destruct required `Metadata` object
   */
  const { metadata }: MetadataData = useStaticQuery(graphql`
    query {
      metadata: contentfulMetadata {
        description
        keywords
        image {
          fixed(width: 512) {
            height
            src
            width
          }
        }
        title
        url
      }
    }
  `)

  return metadata
}
