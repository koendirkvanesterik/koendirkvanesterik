/**
 * Interface representing raw metadata data from data source
 */
export interface MetadataData {
  /**
   * Object containing all metadata properties
   */
  metadata: Metadata
}

export interface Metadata {
  /**
   * String value defining title to be used as title tag
   */
  title: string
  /**
   * String value defining description to be used as metadata
   */
  description: string
  /**
   * String value defining keywords to be used as metadata
   */
  keywords: string
  /**
   * Object containing various image properties
   */
  image: {
    /**
     * Object containing all image data ie. height, src, width, etc.
     */
    fixed: {
      /**
       * String value representing source of image
       */
      src: string
      /**
       * String value representing width of image
       */
      width: string
      /**
       * String value representing height of image
       */
      height: string
    }
  }
  /**
   * String value defining production url of application
   */
  url: string
}
