export interface Props {
  /**
   * String value representing href of page
   */
  href: string
}
