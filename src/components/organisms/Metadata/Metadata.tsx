import React from 'react'
import { Helmet } from 'react-helmet'

import { useMetadata } from './hooks/useMetadata'

export const Metadata: React.FunctionComponent = () => {
  /**
   * Use static query to retrieve default site metadata
   */
  const {
    description,
    image: {
      fixed: { src: imageSrc, width: imageWidth, height: imageHeight },
    },
    keywords,
    title,
    url,
  } = useMetadata()

  return (
    <Helmet>
      <meta name="description" content={description} />
      <meta name="image" content={imageSrc} />
      <meta name="keywords" content={keywords} />
      <meta property="og:description" content={description} />
      <meta property="og:image" content={imageSrc} />
      <meta property="og:image:height" content={imageHeight} />
      <meta property="og:image:width" content={imageWidth} />
      <meta property="og:locale" content="nl" />
      <meta property="og:site_name" content={title} />
      <meta property="og:title" content={title} />
      <meta property="og:type" content="website" />
      <meta property="og:url" content={url} />
      <title>{title}</title>
    </Helmet>
  )
}

Metadata.displayName = 'Metadata'
