import React, { FunctionComponent } from 'react'
import Markdown from 'react-markdown'
import { Link } from 'rebass'
import breaks from 'remark-breaks'

import { Wrapper } from 'components/molecules/Wrapper'

import { useSection } from './hooks/useSection'
import { Props } from './types'

export const Section: FunctionComponent<Props> = ({ name }) => {
  /**
   * Get section by name using static query hook
   */
  const section = useSection(name)

  return (
    section && (
      <Wrapper>
        <Markdown
          plugins={[breaks]}
          renderers={{
            link: ({
              children,
              href,
              target,
            }: HTMLAnchorElement): JSX.Element => (
              <Link href={href} target={target} variant="link">
                {children}
              </Link>
            ),
          }}
        >
          {section.text.text}
        </Markdown>
      </Wrapper>
    )
  )
}

Section.displayName = 'Section'
