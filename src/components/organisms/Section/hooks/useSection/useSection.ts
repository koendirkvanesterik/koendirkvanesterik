import { graphql, useStaticQuery } from 'gatsby'

import { Section, SectionData, SectionNode } from './types'

export const useSection = (name: string): Section | undefined => {
  /**
   * Destruct to edges level, cause that's actually what's required
   */
  const {
    sections: { edges },
  }: SectionData = useStaticQuery(graphql`
    query {
      sections: allContentfulSection {
        edges {
          node {
            name
            text {
              text
            }
          }
        }
      }
    }
  `)

  return (
    edges
      /**
       * We need to transform the result from the GraphQL query, removing all
       * nested raw data in order to make the data easier to manage
       */
      .reduce(
        (sections: readonly Section[], { node }: SectionNode) => [
          ...sections,
          {
            name: node.name,
            text: node.text,
          },
        ],
        [],
      )
      /**
       * Find specific menu based on name ie. `Clients`, `Contact`, etc.
       */
      .find(section => section.name === name)
  )
}
