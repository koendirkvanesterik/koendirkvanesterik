export interface SectionData {
  /**
   * Property representing raw section data
   */
  sections: {
    /**
     * Array of type `SectionNode` representing raw section node data
     */
    edges: readonly SectionNode[]
  }
}

export interface SectionNode {
  /**
   * Object of type `SectionNode` containing raw section node
   */
  node: Section
}

export interface Section {
  /**
   * String value representing name of section
   */
  name: string
  /**
   * Object containing raw text data
   */
  text: {
    /**
     * String value representing markdown text content of section
     */
    text: string
  }
}
