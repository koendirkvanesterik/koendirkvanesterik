export interface Props {
  /**
   * String value representing name of section ie. contact, etc.
   */
  name: string
}
