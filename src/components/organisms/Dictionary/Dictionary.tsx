import React, { createContext, FunctionComponent } from 'react'

import { useLabels } from './hooks/useLabels/useLabels'

import { DictionaryContextProps } from './types'

/**
 * Create and expose `DictionaryContext` to be used in various components
 */
export const DictionaryContext = createContext<DictionaryContextProps>({
  /**
   * Initial value set to guarantee resilience
   */
  labels: {},
})
/**
 * Define `Dictionary` component which will expose all labels from
 * data source to React context
 *
 * @param props of React functional component
 */
export const Dictionary: FunctionComponent = ({ children }) => {
  /**
   * Use static query to retrieve all labels from data source
   */
  const labels = useLabels()
  /**
   * Return context provider with labels array
   */
  return (
    <DictionaryContext.Provider value={{ labels }}>
      {children}
    </DictionaryContext.Provider>
  )
}

Dictionary.displayName = 'Dictionary'
