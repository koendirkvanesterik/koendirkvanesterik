import { graphql, useStaticQuery } from 'gatsby'

import { LabelNode, Labels, LabelsData } from './types'

export const useLabels = (): Labels => {
  /**
   * Destruct to edges level, cause that's actually what's required
   */
  const {
    labels: { edges },
  }: LabelsData = useStaticQuery(graphql`
    query {
      labels: allContentfulLabel {
        edges {
          node {
            key
            value {
              value
            }
          }
        }
      }
    }
  `)
  /**
   * We need to transform the result from the GraphQL query, removing all nested
   * raw data in order to make the data easier manageble
   */
  return edges.reduce(
    (
      labels: Labels,
      {
        node: {
          key,
          value: { value },
        },
      }: LabelNode,
    ) => ({ ...labels, [key]: value }),
    {},
  )
}
