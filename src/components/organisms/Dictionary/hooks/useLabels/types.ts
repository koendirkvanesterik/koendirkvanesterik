export interface LabelsData {
  /**
   * Property representing raw labels data
   */
  labels: {
    /**
     * Array of type `LabelNode` representing raw label node data
     */
    edges: readonly LabelNode[]
  }
}

export interface LabelNode {
  /**
   * Object of type `LabelNode` containing raw label node
   */
  node: Label
}

export interface Label {
  /**
   * String value representing key of label
   */
  key: string
  /**
   * Object containing value of label
   */
  value: {
    /**
     * String value representin value of label
     */
    value: string
  }
}

export interface Labels {
  /**
   * Key-value pair based on type string value representing key of label
   */
  [key: string]: string
}
