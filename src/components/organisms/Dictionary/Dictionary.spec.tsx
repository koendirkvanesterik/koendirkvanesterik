import { shallow } from 'enzyme'
import React from 'react'

import * as hooks from './hooks/useLabels/useLabels'

import { Dictionary } from './Dictionary'

beforeEach(() => {
  const useLabels = jest.spyOn(hooks, 'useLabels')
  useLabels.mockImplementation(() => ({
    foo: 'bar',
  }))
})

describe('Dictionary', () => {
  it('should render as expected', () => {
    const component = shallow(<Dictionary />)
    // expect(component).toMatchSnapshot()
    expect(component.find('ContextProvider').exists()).toBe(true)
    expect(component.find('ContextProvider').prop('value')).toStrictEqual({
      labels: {
        foo: 'bar',
      },
    })
  })
})
