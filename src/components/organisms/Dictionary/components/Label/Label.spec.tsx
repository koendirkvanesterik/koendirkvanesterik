import { mount } from 'enzyme'
import React, { FunctionComponent } from 'react'

import { DictionaryContext } from 'components/organisms/Dictionary'

import { Label } from './Label'

const DictionaryMock: FunctionComponent = ({ children }) => (
  <DictionaryContext.Provider
    value={{
      labels: {
        foo: 'bar',
        baz: '{x} quux',
        quuz: '[corge](/corge/)',
      },
    }}
  >
    {children}
  </DictionaryContext.Provider>
)

describe('Label', () => {
  it('should return matched value based on id', () => {
    const component = mount(
      <DictionaryMock>
        <Label id="foo" />
      </DictionaryMock>,
    )
    expect(component.text()).toEqual('bar')
  })

  it('should return id in case of no match', () => {
    const component = mount(
      <DictionaryMock>
        <Label id="bar" />
      </DictionaryMock>,
    )
    expect(component.text()).toEqual('bar')
  })

  it('should return matched value with replacement based on id', () => {
    const component = mount(
      <DictionaryMock>
        <Label id="baz" replacement={{ x: 'qux' }} />
      </DictionaryMock>,
    )
    expect(component.text()).toEqual('qux quux')
  })

  it('should return matched value with rendered link based on id', () => {
    const component = mount(
      <DictionaryMock>
        <Label id="quuz" />
      </DictionaryMock>,
    )
    expect(component.find('a').prop('href')).toEqual('/corge/')
  })
})
