import { Replacements } from 'utils/string'

export interface Props {
  /**
   * String value representing id of value-pairs in `Dictionary` context
   */
  id: string
  /**
   * Possible object of type `Replacements` that could replace elements within string
   */
  replacement?: Replacements
}
