import React from 'react'
import Markdown from 'react-markdown'
import { Link } from 'rebass'

import { DictionaryContext } from 'components/organisms/Dictionary'

import { replaceString } from 'utils/string'

import { Props } from './types'

export const Label: React.FunctionComponent<Props> = ({ id, replacement }) => (
  <DictionaryContext.Consumer>
    {(context): string | JSX.Element => {
      /**
       * Get label by id from `DictionaryContext`
       */
      const label = context.labels[id]
      /**
       * If label undefined, return id
       */
      if (!label) {
        return id
      }
      /**
       * If replace property is set, replace and return label with given object
       */
      if (replacement) {
        return replaceString(label, replacement)
      }
      /**
       * Check if string contains links, return with markdown wrapper
       */
      if (label.match(new RegExp(/\[.*?\]\(.*?\)/, 'g'))) {
        return (
          <Markdown
            allowedTypes={['text', 'link']}
            renderers={{
              link: ({
                children,
                href,
                target,
              }: HTMLAnchorElement): JSX.Element => (
                <Link href={href} target={target} variant="link">
                  {children}
                </Link>
              ),
            }}
            unwrapDisallowed
          >
            {label}
          </Markdown>
        )
      }
      /**
       * Return label by default
       */
      return label
    }}
  </DictionaryContext.Consumer>
)

Label.displayName = 'Label'
