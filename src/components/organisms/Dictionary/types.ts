import { Labels } from './hooks/useLabels/types'

export interface DictionaryContextProps {
  /**
   * Object of type `Labels` containing all labels sources from data
   */
  labels: Labels
}
