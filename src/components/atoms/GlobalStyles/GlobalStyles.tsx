import { css, Global } from '@emotion/core'
import normalize from 'emotion-normalize'
import React from 'react'

import { colors } from 'components/atoms/Theme/presets/colors'
import { fonts } from 'components/atoms/Theme/presets/fonts'
import { fontSizes } from 'components/atoms/Theme/presets/fontSizes'
import { lineHeights } from 'components/atoms/Theme/presets/lineHeights'

export const GlobalStyles: React.FunctionComponent = () => (
  <Global
    styles={css`
      /**
       * Normalize global css ie. html, body, margins, etc.
       */
      ${normalize}
      /**
       * Set body styles according to specific font styling
       */
      body {
        background-color: ${colors.black};
        color: ${colors.body};
        font-family: ${fonts.body};
        font-size: ${fontSizes[1]}px;
        font-weight: normal;
        line-height: ${lineHeights.body};
        -moz-osx-font-smoothing: grayscale;
        -webkit-font-smoothing: antialiased;
      }
      /**
       * Set specific styles for various elements ie. paragraph, etc.
       */
      p {
        margin-top: 0;
      }
    `}
  />
)

GlobalStyles.displayName = 'GlobalStyles'
