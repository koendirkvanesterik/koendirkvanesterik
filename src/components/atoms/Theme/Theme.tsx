import { ThemeProvider } from 'emotion-theming'
import React from 'react'

import { GlobalStyles } from 'components/atoms/GlobalStyles'

import { presets } from './presets'

/**
 * Create functional component that implements `Rebass` theme component
 * Also include styles normalizing elements across browsers
 *
 * @note An additional `React` fragment is needed to prevent a type issue
 */
export const Theme: React.FunctionComponent = ({ children }) => (
  <ThemeProvider theme={presets}>
    <>
      <GlobalStyles />
      {children}
    </>
  </ThemeProvider>
)

Theme.displayName = 'Theme'
