import { shallow } from 'enzyme'
import React from 'react'

import { Theme } from './Theme'

describe('Theme', () => {
  const component = shallow(<Theme />)
  it('should render as expected', () => {
    expect(component).toMatchSnapshot()
    expect(component.find('ThemeProvider').exists()).toBe(true)
    expect(component.find('GlobalStyles').exists()).toBe(true)
  })
})
