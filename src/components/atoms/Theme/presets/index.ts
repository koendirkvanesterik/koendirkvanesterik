import { breakpoints } from './breakpoints'
import { colors } from './colors'
import { fonts } from './fonts'
import { fontSizes } from './fontSizes'
import { fontWeights } from './fontWeights'
import { lineHeights } from './lineHeights'
import { space } from './space'
import { variants } from './variants'

export const presets = {
  breakpoints,
  colors,
  fonts,
  fontSizes,
  fontWeights,
  lineHeights,
  space,
  variants,
}
