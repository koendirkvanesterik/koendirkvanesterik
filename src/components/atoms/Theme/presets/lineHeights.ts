/**
 * Default line heights
 */
export const lineHeights = {
  /**
   * Line height for body text
   */
  body: 1.5,
  /**
   * Line height for heading text
   */
  heading: 1.1,
}
