/**
 * Default font weights
 */
export const fontWeights = {
  /**
   * Weight for body text elements
   */
  text: 400,
  /**
   * Weight for heading text elements
   */
  heading: 600,
  /**
   * Weight for bold text elements
   */
  bold: 600,
}
