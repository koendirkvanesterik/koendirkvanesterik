const defaultLinkStyles = {
  color: 'body',
  '&:hover': {
    color: 'hover',
  },
}

export const variants = {
  /**
   * Set styles for heading link
   */
  heading: {
    ...defaultLinkStyles,
    textDecoration: 'none',
  },
  /**
   * Set styles for default link
   */
  link: {
    ...defaultLinkStyles,
    '&:hover': {
      color: 'hover',
      textDecoration: 'none',
    },
  },
}
