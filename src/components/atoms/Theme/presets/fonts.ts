/**
 * Default font family used for heading and text components
 */
const DEFAULT_FONT_FAMILY =
  'Gridnik, Geneva, "Helvetica Neue", Helvetica, Arial, sans-serif'
/**
 * Default font stacks
 */
export const fonts = {
  /**
   * Sans-serif fonts for body text
   */
  body: DEFAULT_FONT_FAMILY,
  /**
   * Sans-serif fonts for heading text
   */
  heading: DEFAULT_FONT_FAMILY,
}
