export const baseColors = {
  /**
   * Default base colors
   */
  black: '#000',
  transparent: 'transparent',
  white: '#fff',
  /**
   * Green color range
   * Used for foreground elements ie. text, borders, etc.
   */
  green: ['#28fe14' /** Primary color in this range */],
}

export const colors = {
  ...baseColors,
  /**
   * Set colors for default elements ie. body, heading, etc.
   */
  body: baseColors.green[0],
  /**
   * Set colors for default states ie. hover, etc.
   */
  hover: baseColors.white,
}
