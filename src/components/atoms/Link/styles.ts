import styled from '@emotion/styled'
import { Link } from 'gatsby'
import { variant } from 'styled-system'

import { variants } from 'components/atoms/Theme/presets/variants'

/**
 * This additional styling is needed to align css property value between Rebass-
 * and Gatsby- `Link` components
 */
export const InternalLink = styled(Link)<{
  variant: string
}>(
  {
    boxSizing: 'border-box',
  },
  variant({
    variants,
  }),
)
