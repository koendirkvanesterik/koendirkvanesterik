/**
 * Small util to determine whether href is linking externally
 */
export const isExternalLink = (href: string): boolean =>
  /^(https?:\/\/|mailto:|tel:)/.test(href)

/**
 * Small util to determine target of link
 */
export const setLinkTarget = (href: string): string =>
  /^(https?:\/\/)/.test(href) ? '_blank' : '_self'
