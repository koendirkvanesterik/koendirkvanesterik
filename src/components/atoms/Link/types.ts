import { variants } from 'components/atoms/Theme/presets/variants'

export interface Props {
  /**
   * String value representing actual href of link object
   */
  href: string
  /**
   * Boolean value defining whether to show active style
   */
  showActiveStyle?: boolean
  /**
   * String value representing variant of link
   */
  variant?: keyof typeof variants
  /**
   * On click event handler
   */
  onClick?(event: React.MouseEvent<HTMLAnchorElement>): void
}
