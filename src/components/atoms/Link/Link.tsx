import React from 'react'
import { Link as ExternalLink } from 'rebass'

import { slashSandwich } from 'utils/url'

import { InternalLink } from './styles'
import { Props } from './types'
import { isExternalLink, setLinkTarget } from './utils'

export const Link: React.FunctionComponent<Props> = ({
  children,
  href,
  onClick,
  variant = 'link',
}) => {
  /**
   * Check if link href value matches http(s), mailto, tel protocol and return
   * external `Rebass Link`. If link href value matches http(s) protocol than
   * target _blank otherwise _self
   */
  if (isExternalLink(href)) {
    return (
      <ExternalLink href={href} variant={variant} target={setLinkTarget(href)}>
        {children}
      </ExternalLink>
    )
  }
  /**
   * All other protocols can be considered internal and will use `Gatsby Link`
   * component
   */
  return (
    <InternalLink
      onClick={onClick}
      to={slashSandwich([href])}
      variant={variant}
    >
      {children}
    </InternalLink>
  )
}

Link.displayName = 'Link'
