import { shallow } from 'enzyme'
import React from 'react'

import { Link } from './Link'

describe('Link', () => {
  it('should render Rebass Link component with `_self` target attribute set', () => {
    const component = shallow(<Link href="mailto:lorem@ipsum.dolor" />)
    // expect(component).toMatchSnapshot()
    expect(component.prop('target')).toBe('_self')
  })
  it('should render Rebass Link component with `_blank` target attribute set', () => {
    const component = shallow(<Link href="https://www.lorem.ipsum" />)
    // expect(component).toMatchSnapshot()
    expect(component.prop('target')).toBe('_blank')
  })
  it('should render Gatsby Link component without target attribute', () => {
    const component = shallow(<Link href="/" />)
    // expect(component).toMatchSnapshot()
    expect(component.prop('target')).toBeUndefined()
  })
})
