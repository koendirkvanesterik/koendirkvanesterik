import { shallow } from 'enzyme'
import React from 'react'
import { Box } from 'rebass'

import { Wrapper } from './Wrapper'

describe('Wrapper', () => {
  const component = shallow(<Wrapper />)
  it('should render as expected', () => {
    // expect(component).toMatchSnapshot()
    expect(component.find(Box).exists()).toBeTruthy()
    expect(component.find(Box).prop('margin')).toBe('0 auto')
    expect(component.find(Box).prop('maxWidth')).toBe('560px')
    expect(component.find(Box).prop('paddingX')).toBe(1)
    expect(component.find(Box).prop('width')).toBe('100%')
  })
})
