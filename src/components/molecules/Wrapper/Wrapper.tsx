import React from 'react'
import { Box } from 'rebass'

import { breakpoints } from 'components/atoms/Theme/presets/breakpoints'

import { Props } from './types'

export const Wrapper: React.FunctionComponent<Props> = ({
  children,
  width = 0,
}) => (
  <Box margin="0 auto" maxWidth={breakpoints[width]} paddingX={1} width="100%">
    {children}
  </Box>
)

Wrapper.displayName = 'Wrapper'
