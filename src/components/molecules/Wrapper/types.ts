export interface DefaultProps {
  /**
   * Union type representing index within `breakpoints` array returning width
   */
  width: 0 | 1 | 2 | 3 | 4
}

export type Props = Partial<DefaultProps>
