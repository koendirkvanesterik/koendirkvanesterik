import { combineReducers } from 'redux'

import {
  defaultState as profileState,
  reducers as profileReducers,
} from 'components/organisms/Profile/reducers'
import { sagas as profileSagas } from 'components/organisms/Profile/sagas'

/**
 * Gather all default states from all components
 */
export const defaultState = {
  profile: profileState,
}
/**
 * Combine all reducers from all components
 */
export const reducers = combineReducers({
  profile: profileReducers,
})
/**
 * Combine all sagas from all components within convert feature
 */
export const sagas = [...profileSagas]
