/**
 * Declaring various types within `Window` interface
 */
declare interface Window {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
}

/**
 * Additional Typescript helper type
 * This type will omit selected props from interface
 */
declare type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>
