import React, { ReactNode } from 'react'

import { App } from 'components/organisms/App/App'

interface Props {
  /**
   * Object of type `ReactNode` which holds root element
   */
  element: ReactNode
}

export const wrapRootElement = ({ element }: Props): JSX.Element => (
  <App>{element}</App>
)
