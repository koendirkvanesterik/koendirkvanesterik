import { shallow } from 'enzyme'
import React from 'react'

import { wrapRootElement as Root } from './wrapRootElement'

describe('wrapRootElement', () => {
  const component = shallow(<Root element={<h1>Hello world</h1>} />)
  it('should render as expected', () => {
    // expect(component).toMatchSnapshot()
    expect(component.find('h1').exists()).toBeTruthy()
  })
})
