import { replaceString } from './string'

describe('String utils', () => {
  describe('replaceString', () => {
    const year = new Date().getFullYear()
    it('should replace template with variables in string', () => {
      expect(
        replaceString('© {year} Foo Bar', {
          year,
        }),
      ).toBe(`© ${year} Foo Bar`)
    })
    it('should return string without template if no replacement is given', () => {
      expect(replaceString('© {year} Foo Bar')).toBe('© Foo Bar')
    })
  })
})
