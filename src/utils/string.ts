import isEmpty from 'lodash/isEmpty'

export interface Replacements {
  /**
   * Property with variable key and union type value
   */
  [key: string]: number | string
}

/**
 * Replace string template with given object
 *
 * @access public
 * @param {string} source with to-be-replaced template accolades and key
 * @param {object} replacements with key-pair value matching key in source string
 * @return {string} string with replacement applied
 */
export const replaceString = (
  source: string,
  replacements: Replacements = {},
): string => {
  /**
   * Remove string template if no replacements are give
   */
  if (isEmpty(replacements)) {
    return source.replace(new RegExp('{.*} ', 'g'), '')
  }
  /**
   * Loop through all given keys within object with reduce function to recursively
   * replace all string templates within source string
   */
  return Object.keys(replacements).reduce(
    (replacement, key) =>
      replacement.replace(new RegExp(`{${key}}`, 'g'), `${replacements[key]}`),
    source,
  )
}
