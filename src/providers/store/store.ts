import { Action, applyMiddleware, compose, createStore, Store } from 'redux'
import sagaMiddlewareFactory, { SagaIterator } from 'redux-saga'
import { all } from 'redux-saga/effects'

import {
  defaultState as componentState,
  reducers as componentReducers,
  sagas as componentSagas,
} from 'components'

import { State } from './types'

/**
 * Export configured `Redux` store object
 */
export const store = (): Store => {
  /**
   * Define combined reducers based on each feature
   */
  const reducers = componentReducers
  /**
   * Set initial value of state based on each feature
   */
  const state: State = {
    ...componentState,
  }
  /**
   * Define and run saga functionality
   */
  const sagaMiddleware = sagaMiddlewareFactory()
  /**
   * Define middleware
   */
  const middleware = [sagaMiddleware]
  const appliedMiddleware = applyMiddleware(...middleware)
  /**
   * Compose enhancers - the following will connect `Redux` store to Chrome
   * extension
   */
  const composeEnhancers =
    typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
      ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
      : compose
  /**
   * Define composed enhancers
   */
  const enhancers = composeEnhancers(appliedMiddleware)
  /**
   * Define actual store object with signature set
   */
  const initialStore = createStore<State, Action, void, void>(
    reducers,
    state,
    enhancers,
  )
  /**
   * Define and run sagas after store has been created and its middleware applied
   */
  function* sagas(): SagaIterator {
    yield all([...componentSagas])
  }
  sagaMiddleware.run(sagas)
  /**
   * Return store object
   */
  return initialStore
}
