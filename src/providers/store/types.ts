import { ComponentState } from 'components/types'

/**
 * Object holding all aggregated properties of components state ie. profile,
 * etc.
 */
export type State = ComponentState
