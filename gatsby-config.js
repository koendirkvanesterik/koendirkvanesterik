/**
 * We don't want to commit any secrets to git repository, therefore we use a
 * .env file on every location in order to access this private information
 */
require('dotenv').config()

module.exports = {
  /**
   * Collection of plugins to configure Gatsby
   */
  plugins: [
    /**
     * Catch links rendered in markdown content
     */
    'gatsby-plugin-catch-links',
    /**
     * Add Google Analytics script implementation
     */
    {
      resolve: 'gatsby-plugin-google-analytics',
      options: {
        trackingId: process.env.GA_TRACKING_ID,
      },
    },
    /**
     * Add manifest plugin to render manifest and icons
     */
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        background_color: '#000000',
        display: 'standalone',
        icon: 'static/images/favicon.png',
        name: 'Koen Dirk van Esterik',
        short_name: 'koendirkvanesterik',
        start_url: '/',
        theme_color: '#28fe14',
      },
    },
    /**
     * Add react-helmet to the mix
     */
    'gatsby-plugin-react-helmet',
    /**
     * Add Sharp image manipulation to the mix
     */
    'gatsby-plugin-sharp',
    /**
     * Add typescript stack into webpack
     */
    'gatsby-plugin-typescript',
    /**
     * Implement all required self hosted fonts
     */
    {
      resolve: 'gatsby-plugin-web-font-loader',
      options: {
        classes: false,
        custom: {
          families: ['Gridnik'],
          urls: ['/fonts/fonts.css'],
        },
        events: false,
      },
    },
    /**
     * Implement Contentful as data source
     */
    {
      resolve: 'gatsby-source-contentful',
      options: {
        accessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
        spaceId: process.env.CONTENTFUL_SPACE_ID,
      },
    },
  ],
}
