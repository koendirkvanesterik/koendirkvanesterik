# Koen Dirk van Esterik

Static site based on GatsbyJS

## Development

Global:

- [x] Define project structure
- [ ] Add Google Analytics plugin
- [x] Add Favicon manifest plugin

Atoms:

- [x] Define font library
- [x] Define references constants object

Molecules:

- [x] Develop Footer component
- [x] Develop Header component
- [ ] Develop Helmet component
- [x] Develop Logo component
- [x] Develop Theme component

Organisms:

- [ ] Develop Card component
- [x] Develop Columns component
- [ ] Develop Gallery component
- [ ] Develop Icon component
- [ ] Develop Image component
- [ ] Develop Menu component
- [ ] Develop Paragraph component

## Operations

Testing:

- [x] Script testing commands
- [ ] Integrate Puppeteer package for end-to-end testing

Pipelines:

- [x] Setup CI/CD
- [x] Deploy to pages environment
- [ ] Setup trunk based delopment strategy
- [ ] Setup semver version releases

Domain:

- [x] Point domain to pages environment
- [x] Add SSL certificate
