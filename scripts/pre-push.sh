#!/usr/bin/env bash

cd "$(dirname "$0")/.."
set -e

# Forbid pushing when there are uncommited changes in Git tree
if [[ -n `git status -s`  ]]; then
  echo -e '\x1B[31mThere are uncommited changes in the tree, please stash or commit them before pushing\x1B[0m';
  exit 1;
fi

npx concurrently \
  --kill-others-on-fail \
  "check-dependencies --package-manager npm" \
  "npm run --silent test:types" \
  "npm test -- --changedSince=origin/master"
