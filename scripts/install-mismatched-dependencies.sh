#!/usr/bin/env bash

# Check whether installed npm dependendies match the ones in the package-lock.json, and install mismatched dependencies,
# unless DISABLE_AUTO_INSTALL=true was set on command line or in .env (command line overrides .env).

loadNvm() {
  # Since this hook may have been triggered from a non-terminal context (e.g. by Fork) we need to load nvm explicitly,
  # and can't depend on $PATH.
  HOME_NVM_SH="${HOME}/.nvm/nvm.sh"
  HOMEBREW_NVM_SH="/usr/local/opt/nvm/nvm.sh" 

  export NVM_DIR="$HOME/.nvm"
  if [[ -s $HOME_NVM_SH ]]; then
    . $HOME_NVM_SH
  elif [[ -s $HOMEBREW_NVM_SH ]]; then
    # If there's no nvm.sh in the normal location, check if there's a Homebrew version (assuming a default Homebrew setup).
    . $HOMEBREW_NVM_SH
  else
    echo -e "\x1B[31mERROR: Cannot find nvm in ${HOME_NVM_SH} or ${HOMEBREW_NVM_SH}"
    echo -e 'Auto-install cancelled.\x1B[0m'
    
    # Exit with 0 because we don't want to fail the git hook on a missing nvm (forcing people to set DISABLE_AUTO_INSTALL=true).
    exit 0
  fi

  nvm use
}

# We don't use `check-dependencies --install` because its output is confusing.
mismatchedDeps=$(npx check-dependencies --package-manager npm --color 2>&1 | egrep -v ^Invoke)
if [[ $mismatchedDeps != '' ]]; then
  echo -e "\n\x1B[1;33mInstalled dependencies do not match package-lock.json:\x1B[0m\n\n$mismatchedDeps"
  shouldDisableAutoInstall=$(npx env-cmd -f .env --no-override --use-shell 'echo $DISABLE_AUTO_INSTALL')

  if [[ $shouldDisableAutoInstall == 'true' ]]; then
    echo -e '\n\x1B[31mAuto-install disabled, please run `npm i`.\x1B[0m'
  else
    loadNvm

    echo -e '\n\x1B[92mInstalling mismatched dependencies with `npm i`..\x1B[0m'
    echo -e '\x1B[90m(disable with `DISABLE_AUTO_INSTALL=true` in .env, or at git command, e.g. `DISABLE_AUTO_INSTALL=true git rebase ..`)\x1B[0m\n'
    
    npm i
  fi
fi