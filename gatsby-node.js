const pdf = require('markdown-pdf')
const path = require('path')
const slugify = require('slugify')
/**
 * Modify webpack config; resolve src-folder as root
 * TL;DR https://www.gatsbyjs.org/docs/add-custom-webpack-config/
 */
module.exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    resolve: {
      modules: [path.resolve(__dirname, 'src'), 'node_modules'],
    },
  })
}
/**
 * Create all possible pages
 */
module.exports.createPages = async ({ actions, graphql }) => {
  /**
   * Create landing page on root path
   */
  actions.createPage({
    component: path.resolve(
      `${__dirname}/src/components/pages/Landing/Landing.tsx`,
    ),
    path: '/',
  })
  /**
   * Create not found page on 404 path
   */
  actions.createPage({
    component: path.resolve(
      `${__dirname}/src/components/pages/NotFound/NotFound.tsx`,
    ),
    path: '/404/',
  })
  /**
   * Generate pdf documents from content
   */
  const result = await graphql(`
    {
      documents: allContentfulDocument {
        nodes {
          text {
            text
          }
          name
        }
      }
    }
  `)
  /**
   * Extract raw document text string
   */
  const documents = result.data.documents.nodes
  /**
   * Map through documents array and generate pdf files
   */
  documents.map(({ text: { text }, name }) => {
    /**
     * Set required filename and pathname of pdf document file
     */
    const filename = slugify(name, { lower: true })
    const pathname = `./public/documents/${filename}.pdf`
    /**
     * Generate pdf from text string
     */
    pdf({
      cssPath: './static/styles/document.css',
    })
      .from.string(text)
      .to(pathname, () => {
        console.log(`Generated document: ${pathname}`)
      })
  })
}
